import axios from "axios";

const instance = axios.create({
    // baseURL: "http://localhost:8000/paystar",
    baseURL: "http://94.101.180.121/paystar",
})

instance.defaults.headers.common['withCredentials'] = true;

export default instance
