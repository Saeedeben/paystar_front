import Vue from "vue";
import VueRouter from "vue-router";
import ProductComponent from "@/components/product/ProductComponent";
import CheckoutComponent from "@/components/payment/CheckoutComponent";


Vue.use(VueRouter)

const routes = [
    // API ------------------------------------------------------------------------------------
    {
        path: '/',
        name: 'home',
        component: ProductComponent,
    },

    // Payment ------------------------------------------------------------------------------------
    {
        path: '/paystar/checkout',
        name: 'checkout',
        component: CheckoutComponent,
    },
];

export default new VueRouter({mode: 'history', routes})
